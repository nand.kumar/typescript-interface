import { Injectable, HttpException } from '@nestjs/common';
import { Bike } from 'src/db/entities/bike.entity';
import { User } from 'src/db/entities/user.entity';
import { AddBikeSchema, BikeStatusSchema, ColorSchema, LocationSchema, NameSchema } from 'src/JoiSchema/joiSchema';
import * as jwt from 'jsonwebtoken';
import { Reservation } from 'src/db/entities/reservation.entity';
import * as  moment from 'moment';
import { CreateBike, FilterQuery, GetBikes, UpdateBike } from 'src/dtos/bike.dto';
import { Success } from 'src/dtos/user.dto';
const pageSize = 5;
@Injectable()
export class BikeService {

    async getAllBikes(query: FilterQuery, authtoken: string): Promise<GetBikes> {
        try {
            var decoded = jwt.verify(authtoken, 'bikeReservation');
            const userId = decoded.id;
            const user = await User.findOne({ where: { id: userId } });
            if (user.role === "manager") {
                var bikes = await Bike.find({
                    relations: {
                        reservations: true,
                    }
                });
            }
            if (user.role === "regular") {
                var bikes = await Bike.find();
                bikes = bikes.filter((bike) => bike.isAvailable === true)
            }
            const totalBikes = bikes.length;
            if (query.page && pageSize) {
                bikes = bikes.slice((query.page - 1) * pageSize, query.page * pageSize);
            }
            // console.log(bikes);
            return { bikes, totalBikes, success: true }
        } catch (error) {
            throw new HttpException(error, error.status);

        }
    }

    async getAllFilteredBikes(query: FilterQuery, authtoken: string): Promise<GetBikes> {
        console.log(query);
        try {
            var decoded = jwt.verify(authtoken, 'bikeReservation');
            const userId = decoded.id;
            const user = await User.findOne({ where: { id: userId } });
            if (user.role === "manager") {
                var bikes = await Bike.find({
                    relations: {
                        reservations: true,
                    }
                });
            }
            if (user.role === "regular") {
                var bikes = await Bike.find({
                    relations: {
                        reservations: true,
                    }
                });
                bikes = bikes.filter((bike) => bike.isAvailable === true)
            }
            if (query.rating) {
                bikes = bikes.filter(bike => bike.averageRating >= query.rating);
            }
            if (query.name) {
                bikes = bikes.filter(bike => bike.name.toLowerCase().includes(query.name.toLowerCase()));
            }
            if (query.color) {

                bikes = bikes.filter(bike => bike.color.toLowerCase().includes(query.color.toLowerCase()));
            }
            if (query.location) {

                bikes = bikes.filter(bike => bike.location.toLowerCase().includes(query.location.toLowerCase()));
            }
            if (query.fromDate || query.toDate) {
                if (!query.fromDate || !moment(query.fromDate, 'YYYY-MM-DD H:mm:ss').isValid()) {
                    throw new HttpException('Enter valid from date', 400);
                }
                if (!query.toDate || !moment(query.toDate, 'YYYY-MM-DD H:mm:ss', true).isValid()) {
                    throw new HttpException('Enter valid to date', 400);
                }
                if (!query.fromDate || !query.toDate) {
                    throw new HttpException('Enter valid from and to date', 400);
                }
                if (query.fromDate < moment(Date.now()).format('YYYY-MM-DD H:mm:ss')) {
                    throw new HttpException('Start date should be greater then current date', 400);
                }
                if (query.fromDate > query.toDate) {
                    throw new HttpException('From date cannot be greater than to date', 400);
                }
            }
            if (query.fromDate && query.toDate) {
                bikes = bikes.filter((bike) => {
                    let reservations = bike.reservations;
                    // console.log(bike.name, reservations)
                    reservations = reservations.filter(reservation => reservation.status === true)
                    if (reservations.length === 0) {
                        return true;
                    }
                    let trueCount = 0;
                    for (const reservation of reservations) {

                        if (query.fromDate < reservation.fromDate && query.toDate < reservation.fromDate) {
                            trueCount++;
                        }
                        if ((query.fromDate > reservation.fromDate) && (query.fromDate > reservation.toDate)) {
                            trueCount++;
                        }

                    }
                    if (trueCount === reservations.length) {
                        return true;
                    }
                });
            }
            const totalBikes = bikes.length;
            if (query.page && pageSize) {
                bikes = bikes.slice((query.page - 1) * pageSize, query.page * pageSize);
            }
            if (user.role === "regular") {
                bikes.forEach(bike => {
                    delete bike.reservations;
                });
            }
            return { bikes, totalBikes, success: true }
        } catch (error) {
            console.log(error)
            throw new HttpException(error, error.status);

        }
    }

    async createBike(createBikeData: CreateBike): Promise<Success> {
        try {
            try {
                await AddBikeSchema.validateAsync({ color: createBikeData.color.trim(), location: createBikeData.location.trim(), name: createBikeData.name.trim(), isAvailable: createBikeData.isAvailable });
            } catch (error) {
                throw new HttpException(error.message, 400);
            }
            const bike = new Bike();
            bike.name = createBikeData.name.trim();
            bike.color = createBikeData.color.trim();
            bike.location = createBikeData.location.trim();
            bike.isAvailable = createBikeData.isAvailable;
            await bike.save();
            // console.log(bike);
            return { success: true };
        } catch (error) {
            // console.log(error);
            throw new HttpException(error, error.status);

        }
    }

    async updateBike(id: string, updateBikeData: UpdateBike): Promise<Success> {
        try {
            try {
                if (updateBikeData.name === "" || updateBikeData.name) {
                    await NameSchema.validateAsync({ name: updateBikeData.name.trim() });
                }
                if (updateBikeData.location === "" || updateBikeData.location) {
                    await LocationSchema.validateAsync({ location: updateBikeData.location.trim() });
                }
                if (updateBikeData.color === "" || updateBikeData.color) {
                    await ColorSchema.validateAsync({ color: updateBikeData.color.trim() });
                }
                if (updateBikeData.isAvailable === "" || updateBikeData.isAvailable) {
                    await BikeStatusSchema.validateAsync({ isAvailable: updateBikeData.isAvailable });
                }
            } catch (error) {
                throw new HttpException(error.message, 400);

            }
            const bike = await Bike.findOne({
                where: { id: id }, relations: {
                    reservations: true,
                }
            });
            if (bike) {
                await Bike.update(id, { name: updateBikeData.name.trim(), color: updateBikeData.color.trim(), location: updateBikeData.location.trim(), isAvailable: updateBikeData.isAvailable === "true" });
                const reservations = bike.reservations;
                for (const reservation of reservations) {
                    await Reservation.update(reservation.id, { bikeName: updateBikeData.name.trim() });
                }
                return { success: true }
            }
            else throw new HttpException('Unable to update Bike Invalid bike Id', 400);
        } catch (error) {
            throw new HttpException(error, error.status);


        }
    }

    async deleteBike(id: string): Promise<Success> {
        try {
            const bike = await Bike.findOne({ where: { id: id } });
            if (bike) {
                await Bike.delete(id);
                return { success: true }
            }
            else throw new HttpException('Unable to delete Bike / Bike not Found', 400);
        } catch (error) {
            throw new HttpException(error, error.status);
        }
    }

}